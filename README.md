# Firebase Blog made using React + Firebase

A blog application made using React, Material UI and Firebase. [Hosted Link ](https://blog-e6383.web.app/)

## App Features
- Email and Password Authentication.
- GoogleAuth Authentication.
- Create blog with title and body (only authenticated user can do this).
- View all blogs.
- Hosted using Firebase Hosting.
- Blog and Users data storage in Cloud Firestore.s

## Todo Checklist
- [x] Title search functionality
- [ ] Filter blogs by user and date
- [ ] Advanced filters
- [ ] Sorting functionality
- [ ] Facebook and github auth