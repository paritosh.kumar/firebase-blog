import React, { Component, useState } from 'react';
import './App.css';
import { BrowserRouter as Router,  Route } from 'react-router-dom';
import Navigation from "./components/Navigation/Navigation";
import * as ROUTES from './components/constants/routes';
import SignUpPage from './components/Auth/SignUp'
import Home from './components/Home'
import SignInPage from './components/Auth/SignIn'
import CreateBlog from './components/Blog/CreateBlog'
import { withAuthentication } from './components/Session';
import Footer from './components/Footer';


const App = () => {

  const [filter,setFilter] = useState('')

  const changeFilter = (text) => {
    setFilter(text)
  }
  
  return (

    <Router>
    <div className="App">
      <Navigation changeFilter={changeFilter}/>
      <div className="content">
      <Route path={ROUTES.HOME} exact component={()=><Home filterField={filter} />} />
      <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
      <Route path={ROUTES.SIGN_IN} component={SignInPage} />
      <Route path={ROUTES.CREATE_BLOG} component={CreateBlog} />
      </div>
      <Footer />
    </div>
    </Router>
  );
}

export default withAuthentication(App);
