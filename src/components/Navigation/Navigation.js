import React,{useState} from "react"
import { Link } from 'react-router-dom';
import * as ROUTES from '../constants/routes';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Box, Grid,Container} from '@material-ui/core'
import { fade, makeStyles } from '@material-ui/core/styles';
import { AuthUserContext } from '../Session';
import SignOutButton from '../Auth/SignOut';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import AddCircleOutlineTwoToneIcon from '@material-ui/icons/AddCircleOutlineTwoTone';

const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  }
}));

const Navigation = (props) => (
  <div>
   <AuthUserContext.Consumer>
   {authUser =>
        authUser ? <NavigationAuth changeFilter={props.changeFilter}/> : <NavigationNonAuth changeFilter={props.changeFilter}/>
      }
     </AuthUserContext.Consumer>
     </div>
);

const SearchField = (props) => {
  const [search,setSearch] = useState('')
  React.useEffect(()=>{
    props.changeFilter(search)
  })
  const classes = useStyles();
  return(
    <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search for title…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
              value={search}
              onChange={e=>{setSearch(e.target.value);}}
            />
          </div>
  )
}

const NavigationAuth  = (props) =>  {
    return(
      <AppBar position="static" color="transparent">
        <Container disableGutters>
        <Toolbar>
          

        <Box mx={2} className="appbar-logo">
          <Button color="primary" size="large"><Link to={ROUTES.HOME} color="primary" >Blogs</Link></Button>
          </Box>
          <SearchField changeFilter={props.changeFilter}/>
          <Box mx={2}>
          <Button startIcon={<AddCircleOutlineTwoToneIcon />} variant="contained" color="primary" disableElevation><Link to={ROUTES.CREATE_BLOG} style={{color:"white"}}>Create Blog</Link></Button>
          </Box>
          
          <Box mx={2}>
         <SignOutButton />
         </Box>
          </Toolbar>
          </Container>
      </AppBar>
      
    )
}

const NavigationNonAuth  = (props) =>  {
    return(
      <AppBar position="static" color="transparent">
        <Container disableGutters>
        <Toolbar>
        <Box mx={2} className="appbar-logo">
          <Button color="primary" size="large"><Link to={ROUTES.HOME} color="primary" >Blogs</Link></Button>
          </Box>
          <SearchField changeFilter={props.changeFilter}/>
          <Box mx={2}>
          <Button color="primary"><Link to={ROUTES.SIGN_IN} color="primary">Sign In</Link></Button>
          </Box>
          </Toolbar>
          </Container>
          
      </AppBar>
      
    )
}
export default Navigation;