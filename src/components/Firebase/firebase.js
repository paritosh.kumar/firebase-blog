import app from 'firebase/app'
import 'firebase/auth';
import 'firebase/firestore';
import * as firebase from 'firebase';


const config = {
    apiKey: "AIzaSyCFwvFiYQ8vk5tr86xfKM9i7ekH43NEnuA",
    authDomain: "blog-e6383.firebaseapp.com",
    databaseURL: "https://blog-e6383.firebaseio.com",
    projectId: "blog-e6383",
    storageBucket: "blog-e6383.appspot.com",
    messagingSenderId: "542826504801",
    appId: "1:542826504801:web:e8c6120a24d99f46e94020",
    measurementId: "G-97Z7LX0TXD"
  };

  export default class Firebase{
      constructor(){
          app.initializeApp(config)
          this.auth = app.auth();
          this.db = app.firestore();
      }

      // Auth API
      doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignInWithGoogleAuth = () => {
        let provider = new firebase.auth.GoogleAuthProvider()
        return this.auth.signInWithPopup(provider)
    }

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);
    // *** User API ***
 
    user = uid => this.db.doc(`users/${uid}`);
    
    users = () => this.db.collection('users');
    blogs = () => this.db.collection('blogs');
  }