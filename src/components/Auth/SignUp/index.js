import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import {TextField, Button, Grid, Box} from '@material-ui/core';
import { withFirebase } from '../../Firebase'
import * as ROUTES from '../../constants/routes';
 
const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
  };

const SignUpPage = () => (
  <div>
    <h1 className="text-align-center">Sign Up</h1>
    <SignUpForm />
  </div>
);
 
class SignUpFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  googleSignUp = event => {
    this.props.firebase.doSignInWithGoogleAuth()
    .then(authUser => {
      return this.props.firebase
        .users().doc(authUser.user.uid)
        .set({
          username:authUser.user.displayName,
          email:authUser.user.email,
          photoURL:authUser.user.photoURL,
          providerId:authUser.user.providerId
        })
    })
    .then(()=>{
      this.props.history.push(ROUTES.HOME);
    })
    .catch(error => {
      this.setState({ error });
    });
    event.preventDefault();
  }
 
  onSubmit = event => {
    const { username, email, passwordOne } = this.state;
 
    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in your Firebase realtime database
        return this.props.firebase
          .users().doc(authUser.user.uid)
          .set({
            username,
            email,
          });
      })
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });
 
    event.preventDefault();

  }
 
  render() {
    const {
        username,
        email,
        passwordOne,
        passwordTwo,
        error,
      } = this.state;

      const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      username === '';

    return (
      <form onSubmit={this.onSubmit}>
        <Grid
        container
        direction="column"
        justify="center"
        alignItems="center">
        <Box my={2}><TextField 
          margin="dense"
          name="username"
          value={username}
          onChange={this.onChange}
          type="text"
          label="Full Name"
          variant="outlined"
        /></Box>
        <Box my={2}><TextField
          margin="dense"
          name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          label="Email Address"
          variant="outlined"
        /></Box>
        <Box my={2}><TextField
          margin="dense"
          name="passwordOne"
          value={passwordOne}
          onChange={this.onChange}
          type="password"
          label="Password"
          variant="outlined"
        /></Box>
        <Box my={2}><TextField
          margin="dense"
          name="passwordTwo"
          value={passwordTwo}
          onChange={this.onChange}
          type="password"
          label="Confirm Password"
          variant="outlined"
        /></Box>
        <Box my={2}><Button disabled={isInvalid} type="submit" variant="contained" color="primary" >
            Sign Up
        </Button>
        </Box>
        <Box my={2}>
        <Button onClick={this.googleSignUp} type="submit" variant="contained" color="secondary">
          Sign Up With Google
        </Button>
        </Box>
 
        {error && <p>{error.message}</p>}
        </Grid>
      </form>
    );
  }
}

const SignUpForm = withRouter(withFirebase(SignUpFormBase));
 
const SignUpLink = () => (
  <p className="text-align-center">
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);
 
export default SignUpPage;
 
export { SignUpForm, SignUpLink };