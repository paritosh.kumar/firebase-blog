import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { SignUpLink } from '../SignUp';
import {TextField, Button, Grid, Box} from '@material-ui/core';
import { withFirebase } from '../../Firebase';
import * as ROUTES from '../../constants/routes';

const SignInPage = () => (
  <div>
    <h1 className="text-align-center">Sign In</h1>
    <SignInForm />
    <SignUpLink />
  </div>
);
const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
  };

  class SignInFormBase extends Component {
    constructor(props) {
      super(props);
   
      this.state = { ...INITIAL_STATE };
    }
   
    onSubmit = event => {
      const { email, password } = this.state;
   
      this.props.firebase
        .doSignInWithEmailAndPassword(email, password)
        .then(() => {
          this.setState({ ...INITIAL_STATE });
          this.props.history.push(ROUTES.HOME);
        })
        .catch(error => {
          this.setState({ error });
        });
   
      event.preventDefault();
    };

    googleSignIn = event => {
      this.props.firebase.doSignInWithGoogleAuth()
      .then(authUser => {
        return this.props.firebase
          .users().doc(authUser.user.uid)
          .set({
            username:authUser.user.displayName,
            email:authUser.user.email,
            photoURL:authUser.user.photoURL,
            providerId:authUser.user.providerId
          })
      })
      .then(()=>{
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });
      event.preventDefault();
    }
   
    onChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
   
    render() {
      const { email, password, error } = this.state;
   
      const isInvalid = password === '' || email === '';
   
      return (
        <form onSubmit={this.onSubmit}>
             <Grid
             
        container
        direction="column"
        justify="space-between"
        fullWidth
        alignItems="center"
        spacing={2}>
          <Box my={2}>
          <TextField
            name="email"
            value={email}
            onChange={this.onChange}
            type="text"
            label="Email Address"
            variant="outlined"
            margin="dense"
          />
          </Box>
          <Box my={2}>
          <TextField
            name="password"
            value={password}
            onChange={this.onChange}
            type="password"
            label="Password"
            variant="outlined"
            margin="dense"
          />
          </Box>

         
          <Button disabled={isInvalid} type="submit" variant="contained" color="primary">
            Sign In
          </Button>
          <Box my={2}>
          <Button  variant="contained" color="secondary" onClick={this.googleSignIn}>Sign in with google</Button>
          </Box>
   
          {error && <p>{error.message}</p>}
          </Grid>
        </form>
      );
    }
  }
   
  const SignInForm = withRouter(withFirebase(SignInFormBase));
   
  export default SignInPage;
   
  export { SignInForm };