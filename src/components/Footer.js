import React from "react"
import {Typography} from "@material-ui/core"

export default function Footer() {
    return(
        <footer>
<Typography variant="caption" gutterBottom display="block">
    Made By: Paritosh Kumar
</Typography>
<Typography variant="caption" gutterBottom display="block">
    Source Link: <a href="https://gitlab.com/paritosh.kumar/firebase-blog" target="_blank">gitlab@paritosh.kumar</a>
</Typography>
        </footer>
    )
}