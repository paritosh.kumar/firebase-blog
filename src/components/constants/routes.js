
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const HOME = '/';
export const BLOG = '/blog/<id>'
export const CREATE_BLOG = '/create';
export const PASSWORD_FORGET = '/pw-forget';