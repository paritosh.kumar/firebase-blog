import React,{useState} from "react";
import {Grid, Typography, Box} from '@material-ui/core'
import {withFirebase} from '../Firebase'
import {Link} from "react-router-dom";
import CalendarTodayTwoToneIcon from '@material-ui/icons/CalendarTodayTwoTone';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';

export default withFirebase(function BlogThumbnail(props){
    const [username,setUsername] = useState(null)

    const data = props.data.data() || {title:'test',author_id:'test_id'}
    const date = data.timestamp.toDate()
    
    props.firebase.users().doc(data.author_id.id).get().then(doc=>doc.data()).then(data=>setUsername(data.email))
return(
    <Grid item xs={12} lg={6} className="blog-thumbnail" >
        <Box my={5} px={3}>
        <Typography variant="h4">
            {data.title.split(' ').map(word=>word.charAt(0).toUpperCase()+word.slice(1)).join(' ')}
        </Typography>
        <Typography variant="body2">
            {data.body}
        </Typography>
        <Box display="flex" alignItems="center">
        <AccountCircleTwoToneIcon fontSize="small"/>
        <Box ml={1}>
        <Typography variant="caption">
            {username}
        </Typography>
        </Box>
        </Box>
        
        <Box display="flex" alignItems="center" mt={1}>
        <CalendarTodayTwoToneIcon fontSize="small"/>
        <Box ml={1}>
        <Typography variant="caption">
           {`${date.toDateString()} | ${date.toLocaleTimeString()}`}
        </Typography>
        </Box>
        </Box>
        {/* <Link to={`blog/${data.title.split(' ').slice(0,3).join('-')}`} >View Blog</Link> */}
        </Box>
    </Grid>
)
})

