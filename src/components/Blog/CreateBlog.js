import React,{useState} from "react"
import { AuthUserContext, withAuthorization } from '../Session';
import {TextField, Button, Container, Box} from '@material-ui/core';
import {HOME} from '../constants/routes'

function CreateBlog(props) {

  const [title,setTitle] = useState('')
  const [body,setBody] = useState('')
  const [errorMessage,setError] = useState(null)

  const createBlog = (authUser) => {
    if(title.length>1 && body.length>1){
      props.firebase.blogs().add({
        title:title,
        body:body,
        timestamp: new Date(),
        author_id:props.firebase.user(authUser.uid)
      })
      props.history.push(HOME)
      
    }
    else{
      setError('All fields required')
    }
    
  }





  
    return(
        <AuthUserContext.Consumer>
    {authUser => (
      <div>
        <Container>
        <h1 className="text-align-center">Create Blog</h1>
        <form>
        <Box my={5}>
        <TextField required value={title} onChange={e=>setTitle(e.target.value)} label="Title" variant="outlined" required size="large" fullWidth/>
        </Box>
        <Box my={3}>
        <TextField required rows="5" value={body} onChange={e=>setBody(e.target.value)} label="Body" variant="outlined" required size="large" fullWidth multiline/>
        </Box>
        <Button variant="contained" onClick={()=>createBlog(authUser)} color="primary" size="large">
          Create Blog
        </Button>
        </form>
        </Container>
      </div>
    )}
  </AuthUserContext.Consumer>
    )
}
const condition = authUser => !!authUser;
export default withAuthorization(condition)(CreateBlog)