import React, {useState, useEffect} from 'react'
import {withFirebase} from './Firebase';
import { withRouter } from 'react-router-dom';
import {Grid, Container} from '@material-ui/core'
import BlogThumbnail from './Blog/BlogThumbnail'; 
class HomeBase extends React.Component {
    constructor(props){
        super(props);
        this.state={
            blogs:[],
        }
    }

    componentDidMount = () => {
        this.props.firebase.blogs().orderBy("timestamp","desc").get().then(blogs => { this.setState({blogs:blogs})});
    }


    
      
render(){
    const blogs = []
    const filter = this.props.filterField
    console.log(filter,filter.length)
    
  this.state.blogs.forEach(blog => {
        blogs.push(<BlogThumbnail data={blog}/>)
    })
    console.log(blogs.filter((blog) => {
        const title = blog.props.data.data().title || '';
        return title.includes(filter)
    }))
  
    return(
        <div>
            <Container>
            <Grid container>
            { filter.length > 1 ? blogs.filter((blog) => {
        const title = blog.props.data.data().title || '';
        return title.includes(filter)
    }) : blogs}
            </Grid>
            </Container>
        </div>
    )
}
}

const Home = withRouter(withFirebase(HomeBase))

export default Home